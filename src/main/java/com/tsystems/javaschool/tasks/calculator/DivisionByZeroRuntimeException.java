package com.tsystems.javaschool.tasks.calculator;

public class DivisionByZeroRuntimeException extends RuntimeException{
    public DivisionByZeroRuntimeException() {
        super();
    }
}
