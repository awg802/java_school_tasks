package com.tsystems.javaschool.tasks.calculator;

enum TokenType {

    LEFT_BRACKET,
    RIGHT_BRACKET,
    NUMBER,
    SUBSTRACTION,
    ADDITION,
    MULTIPLICATION,
    DIVISION
}
