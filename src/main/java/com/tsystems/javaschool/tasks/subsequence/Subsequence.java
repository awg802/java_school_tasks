package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int xElem = 0;
        int yElem = 0;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() > y.size()) {
            return false;
        }
        while (xElem < x.size()) {
            Object Xitem = x.get(xElem);
            Object Yitem = y.get(yElem);
            while (!Xitem.equals(Yitem)) {
                if (++yElem > y.size() - 1) {
                    return false;
                }
                Yitem = y.get(yElem);
            }
            xElem++;
        }
        return true;
    }

}
