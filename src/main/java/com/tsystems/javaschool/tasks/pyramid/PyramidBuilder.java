package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int size = inputNumbers.size();
        if (size > Integer.MAX_VALUE - 2) {
            throw new CannotBuildPyramidException();
        }
        int columns = 1;
        int rows = 1;
        int quantity = 0;
        while (quantity < size) {
            quantity += rows;
            rows++;
            columns += 2;
        }
        rows -= 1;
        columns -= 2;
        for (int i = 0; i < size; i++) {
            if (inputNumbers.get(i) == null) {
                throw new CannotBuildPyramidException();
            }
        }
        if (quantity != size) {
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);

        int[][] pyramid = new int[rows][columns];

        int center = (columns / 2);
        int count = 1;
        int offset = 0;
        int listIndex = 0;
        for (int i = 0; i < rows; i++) {
            int start = center - offset;
            for (int j = 0; j < count * 2; j += 2) {

                pyramid[i][start + j] = inputNumbers.get(listIndex);
                listIndex++;
            }
            offset++;
            count++;
        }
        return pyramid;
    }


}
